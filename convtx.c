#include    <wb.h>

#define wbCheck(stmt) do {                                                    \
        cudaError_t err = stmt;                                               \
        if (err != cudaSuccess) {                                             \
            wbLog(ERROR, "Failed to run stmt ", #stmt);                       \
            wbLog(ERROR, "Got CUDA error ...  ", cudaGetErrorString(err));    \
            return -1;                                                        \
        }                                                                     \
    } while(0)

//@@ INSERT CODE HERE

#define MASK_WIDTH  5
#define MASK_RADIUS MASK_WIDTH/2
#define TILE_WIDTH 16
#define O_TILE_WIDTH (TILE_WIDTH + MASK_WIDTH - 1)
//#define clamp(x, start, end) (min(max((x), start), end))
#define clamp(x) (min(max((x), 0.0), 1.0))


__global__ void convolution_2D_kernel(float *P, float *N, int height,
                  int width, int channels, const float * __restrict__ M) {

	int k;
    float sum = 0.0;
    int i, j, x, y, xOffset, yOffset;
    int tx = threadIdx.x;
    int ty = threadIdx.y;
	int row_o = blockIdx.y*O_TILE_WIDTH + ty;
    int col_o = blockIdx.x*O_TILE_WIDTH + tx;
    int row_i = row_o - MASK_RADIUS;
	int col_i = col_o - MASK_RADIUS;
    __shared__ float Ns[O_TILE_WIDTH][O_TILE_WIDTH];
    j = row_o;
    i = col_o;
	P[0] = 0.22;
	P[1] = 0.33;

    for (k = 0; k < channels; k++) {
        if((row_i >= 0) && (row_i < height) && (col_i >= 0) && (col_i < width) ) {
             Ns[ty][tx] = N[(row_i*width + col_i)*channels + k];    // populated by all threads from same block
        } 
		else {
             Ns[ty][tx] = 0.0f;
        }
     }
	 __syncthreads();
	
    for (k = 0; k < channels; k++) {
		sum = 0.0f;
		for (y = 0; y < MASK_WIDTH; y++) {
             for (x = 0; x < MASK_WIDTH; x++) {
                 sum += Ns[ty][tx] * M[y*MASK_WIDTH + x];
			 }		
        }
        // pixels are in the range of 0 to 1
		if (i < height && j < width)
//           P[(i * width + j)*channels + k] = clamp(sum, 0.0, 1.0);
           P[(i * width + j)*channels + k] = clamp(sum);
    }
	__syncthreads();

}




int main(int argc, char* argv[]) {
    wbArg_t args;
    int maskRows;
    int maskColumns;
    int imageChannels;
    int imageWidth;
    int imageHeight;
    char * inputImageFile;
    char * inputMaskFile;
    wbImage_t inputImage;
    wbImage_t outputImage;
    float * hostInputImageData;
    float * hostOutputImageData;
    float * hostMaskData;
    float * deviceInputImageData;
    float * deviceOutputImageData;
    float * deviceMaskData;

    args = wbArg_read(argc, argv); /* parse the input arguments */

    inputImageFile = wbArg_getInputFile(args, 0);
    inputMaskFile = wbArg_getInputFile(args, 1);

    inputImage = wbImport(inputImageFile);
    hostMaskData = (float *) wbImport(inputMaskFile, &maskRows, &maskColumns);

    assert(maskRows == 5); /* mask height is fixed to 5 in this mp */
    assert(maskColumns == 5); /* mask width is fixed to 5 in this mp */

    imageWidth = wbImage_getWidth(inputImage);
    imageHeight = wbImage_getHeight(inputImage);
    imageChannels = wbImage_getChannels(inputImage);

    outputImage = wbImage_new(imageWidth, imageHeight, imageChannels);

    hostInputImageData = wbImage_getData(inputImage);
    hostOutputImageData = wbImage_getData(outputImage);

    wbTime_start(GPU, "Doing GPU Computation (memory + compute)");

    wbTime_start(GPU, "Doing GPU memory allocation");
    cudaMalloc((void **) &deviceInputImageData, imageWidth * imageHeight * imageChannels * sizeof(float));
    cudaMalloc((void **) &deviceOutputImageData, imageWidth * imageHeight * imageChannels * sizeof(float));
    cudaMalloc((void **) &deviceMaskData, maskRows * maskColumns * sizeof(float));
    wbTime_stop(GPU, "Doing GPU memory allocation");


    wbTime_start(Copy, "Copying data to the GPU");
    cudaMemcpy(deviceInputImageData,
               hostInputImageData,
               imageWidth * imageHeight * imageChannels * sizeof(float),
               cudaMemcpyHostToDevice);
    cudaMemcpy(deviceMaskData,
               hostMaskData,
               maskRows * maskColumns * sizeof(float),
               cudaMemcpyHostToDevice);
    wbTime_stop(Copy, "Copying data to the GPU");


    wbTime_start(Compute, "Doing the computation on the GPU");
    //@@ INSERT CODE HERE

	dim3 dimGrid(ceil((float)imageWidth/TILE_WIDTH), ceil((float)imageHeight/TILE_WIDTH));
    dim3 dimBlock(TILE_WIDTH, TILE_WIDTH, 1);

	// dim3 dimBlock(BLOCK_WIDTH,BLOCK_WIDTH);
    // dim3 dimGrid((wbImage_getWidth(deviceInputImageData)-1)/O_TILE_WIDTH+1, (wbImage_getHeight(deviceInputImageData)-1)/O_TILE_WIDTH+1, 1);
	convolution_2D_kernel<<<dimGrid,dimBlock>>> (deviceOutputImageData, deviceInputImageData,
												 imageHeight, imageWidth, imageChannels, deviceMaskData);

    wbTime_stop(Compute, "Doing the computation on the GPU");


    wbTime_start(Copy, "Copying data from the GPU");
    cudaMemcpy(hostOutputImageData,
               deviceOutputImageData,
               imageWidth * imageHeight * imageChannels * sizeof(float),
               cudaMemcpyDeviceToHost);
    wbTime_stop(Copy, "Copying data from the GPU");

    wbTime_stop(GPU, "Doing GPU Computation (memory + compute)");

    wbSolution(args, outputImage);

    cudaFree(deviceInputImageData);
    cudaFree(deviceOutputImageData);
    cudaFree(deviceMaskData);

    free(hostMaskData);
    wbImage_delete(outputImage);
    wbImage_delete(inputImage);

    return 0;
}
