#include <wb.h> //@@ wb include opencl.h for you

//@@ OpenCL Kernel

// Allocate device memory using clCreateBuffer
// Host to device data copy using clEnqueueWriteBuffer
// Prepare kernel parameters and launch kernel
// Device to host data copy using clEnqueueReadBuffer
// Write the OpenCL kernel code

int main(int argc, char **argv) {
  wbArg_t args;
  int inputLength;
  float *hostInput1;
  float *hostInput2;
  float *hostOutput;
  float *deviceInput1;
  float *deviceInput2;
  float *deviceOutput;

  args = wbArg_read(argc, argv);

  wbTime_start(Generic, "Importing data and creating memory on host");
  hostInput1 = ( float * )wbImport(wbArg_getInputFile(args, 0), &inputLength);
  hostInput2 = ( float * )wbImport(wbArg_getInputFile(args, 1), &inputLength);
  hostOutput = ( float * )malloc(inputLength * sizeof(float));
  wbTime_stop(Generic, "Importing data and creating memory on host");

  wbLog(TRACE, "The input length is ", inputLength);

  wbTime_start(GPU, "Allocating GPU memory.");
  //@@ Allocate GPU memory here

   cl_platform_id cpPlatform;
   cl_device_id cdDevice;

   cl_int clerr;
   cl_context clctx;
   cl_program clpgm;

// Get an OpenCL platform
  clerr = clGetPlatformIDs(1, &cpPlatform, NULL);

// Get the devices
  clerr = clGetDeviceIDs(cpPlatform, CL_DEVICE_TYPE_GPU, 1, &cdDevice, NULL);

// Create the context
  clctx = clCreateContext(0, 1, &cdDevice, NULL, NULL, &clerr);

  cl_command_queue clcmdq = clCreateCommandQueue(clctx, cdDevice, 0, &clerr);

  cl_mem d_A, d_B, d_C;
  int size = inputLength* sizeof(float);
  d_A = clCreateBuffer(clctx, CL_MEM_READ_ONLY, size, NULL, NULL);
  d_B = clCreateBuffer(clctx, CL_MEM_READ_ONLY, size, NULL, NULL);
  d_C = clCreateBuffer(clctx, CL_MEM_READ_WRITE, size, NULL, NULL);

  wbTime_stop(GPU, "Allocating GPU memory.");

  wbTime_start(GPU, "Copying input memory to the GPU.");
  //@@ Copy memory to the GPU here

  clEnqueueWriteBuffer(clcmdq, d_A, CL_FALSE, 0, size, (const void * )hostInput1, 0, 0, NULL);
  clEnqueueWriteBuffer(clcmdq, d_B, CL_FALSE, 0, size, (const void * )hostInput2, 0, 0, NULL);

  wbTime_stop(GPU, "Copying input memory to the GPU.");

  //@@ Initialize the grid and block dimensions here

  wbTime_start(Compute, "Performing CUDA computation");
  //@@ Launch the GPU Kernel here

   static const char *vecAddKernel =
	"__kernel void vadd(__global const float *d_A, __global const float *d_B, __global float *d_C, int N) {\
	    int id = get_global_id(0);\
	if (id < N)\
    d_C[id] = d_A[id] + d_B[id];\
	}";

   clpgm = clCreateProgramWithSource(clctx, 1, &vecAddKernel,NULL, &clerr);

   char clcompileflags[4096];
   sprintf(clcompileflags, "-cl-mad-enable");
   clerr = clBuildProgram(clpgm, 0, NULL, clcompileflags, NULL, NULL);
   cl_kernel clkern = clCreateKernel(clpgm, "vadd", &clerr);

   clerr = clSetKernelArg(clkern, 0, sizeof(cl_mem), (void *)&d_A);
   clerr = clSetKernelArg(clkern, 1, sizeof(cl_mem), (void *)&d_B);
   clerr = clSetKernelArg(clkern, 2, sizeof(cl_mem), (void *)&d_C);
   clerr = clSetKernelArg(clkern, 3, sizeof(int), &inputLength);


// get_global_size(0); size of NDRange in the x dimension gridDim.x*blockDim.x
// get_local_size(0); Size of each work group in the x dimension blockDim.x

   size_t Bsz = 32;
   size_t numGrid = inputLength/Bsz;
   if (inputLength % Bsz > 0) numGrid++;
   size_t Gsz = numGrid*Bsz;


   cl_event event=NULL;
   clerr = clEnqueueNDRangeKernel(clcmdq, clkern, 1, NULL, &Gsz, &Bsz, 0, NULL, &event);
   clerr = clWaitForEvents(1, &event);

  //cudaDeviceSynchronize();
  wbTime_stop(Compute, "Performing CUDA computation");

  wbTime_start(Copy, "Copying output memory to the CPU");
  //@@ Copy the GPU memory back to the CPU here

  clEnqueueReadBuffer(clcmdq, d_C, CL_TRUE, 0, size, hostOutput, 0, NULL, NULL);

  wbTime_stop(Copy, "Copying output memory to the CPU");

  wbTime_start(GPU, "Freeing GPU Memory");
  //@@ Free the GPU memory here

  clReleaseMemObject(d_A);
  clReleaseMemObject(d_B);
  clReleaseMemObject(d_C);

  wbTime_stop(GPU, "Freeing GPU Memory");

  wbSolution(args, hostOutput, inputLength);

  free(hostInput1);
  free(hostInput2);
  free(hostOutput);

  return 0;
}
