#include <wb.h>

#define wbCheck(stmt) do {                                                    \
        cudaError_t err = stmt;                                               \
        if (err != cudaSuccess) {                                             \
            wbLog(ERROR, "Failed to run stmt ", #stmt);                       \
            wbLog(ERROR, "Got CUDA error ...  ", cudaGetErrorString(err));    \
            return -1;                                                        \
        }                                                                     \
    } while(0)

__global__ void vecAdd(float *in1, float *in2, float *out, int len) {
  //@@ Insert code to implement vector addition here

    int ii = blockIdx.x*blockDim.x+threadIdx.x;

    if (ii < len)
        out[ii] = in1[ii] + in2[ii];
}

#define BLOCK_SIZE 128
#define NOSTREAMS 4

char *msg(char *s, int i) {
	char *s1;
	s1 = (char *) malloc(100);
	sprintf(s1, "%s %d", s, i);
    return s1;
}

int main(int argc, char **argv) {
    wbArg_t args;
    int inputLength;
    float *h_A;
    float *h_B;
    float *h_C;
    float *d_A[100], *d_B[100], *d_C[100];// device memory for streams
    int segSize;

    args = wbArg_read(argc, argv);

    wbTime_start(Generic, "Importing data and creating memory on host");
    h_A=  ( float * )wbImport(wbArg_getInputFile(args, 0), &inputLength);
    h_B = ( float * )wbImport(wbArg_getInputFile(args, 1), &inputLength);
    h_C = ( float * )malloc(inputLength * sizeof(float));
    wbTime_stop(Generic, "Importing data and creating memory on host");

    wbLog(TRACE, "The input length is ", inputLength);

 //@@ STREAMS

	segSize = inputLength/NOSTREAMS;
    wbLog(TRACE, "The segment size is ", segSize);

    wbTime_start(Generic, "Creating streams");
    cudaStream_t stream[100];
	for (int i=0; i< NOSTREAMS; i++)
         wbCheck( cudaStreamCreate(&stream[i]) );
    wbTime_stop(Generic, "Creating streams");

//@@ cudaMalloc for d_A0, d_B0, d_C0, d_A1, d_B1, d_C1 .... go here

    //@@ Alocating GPU memory

	wbTime_start(GPU, "Allocating GPU memory.");
	for (int i=0; i< NOSTREAMS; i++) {
        wbCheck( cudaMalloc((void **) &d_A[i], segSize* sizeof(float)) );
        wbCheck( cudaMalloc((void **) &d_B[i], segSize* sizeof(float)) );
        wbCheck( cudaMalloc((void **) &d_C[i], segSize* sizeof(float)) );
    }
    wbTime_stop(GPU, "Allocating GPU memory.");


	//@@ Initialize the grid and block dimensions here

    int bb, gg, i;

	bb = BLOCK_SIZE;
    gg = (int)ceil((float)segSize/BLOCK_SIZE);

	i = 0;
    wbTime_start(GPU, msg("Copying input memory to the GPU - Stream", i));
    wbCheck( cudaMemcpyAsync(d_A[i], h_A + i*segSize, segSize*sizeof(float), cudaMemcpyHostToDevice, stream[i]) );
    wbCheck( cudaMemcpyAsync(d_B[i], h_B + i*segSize, segSize*sizeof(float), cudaMemcpyHostToDevice, stream[i]) );
    wbTime_stop(GPU, msg("Copying input memory to the GPU - Stream", i));

	wbCheck( cudaStreamSynchronize(stream[i]) );

	wbTime_start(Compute, msg("Performing CUDA computation - Stream", i));
    vecAdd<<<gg, bb, 0, stream[i]>>>(d_A[i], d_B[i], d_C[i], segSize);
    wbTime_stop(Compute, msg("Performing CUDA computation - Stream", i));

	i = 1;
    wbTime_start(GPU, msg("Copying input memory to the GPU - Stream", i));
    wbCheck( cudaMemcpyAsync(d_A[i], h_A + i*segSize, segSize*sizeof(float), cudaMemcpyHostToDevice, stream[i]) );
    wbCheck( cudaMemcpyAsync(d_B[i], h_B + i*segSize, segSize*sizeof(float), cudaMemcpyHostToDevice, stream[i]) );
    wbTime_stop(GPU, msg("Copying input memory to the GPU - Stream", i));

	wbCheck( cudaStreamSynchronize(stream[i-1]) );
    wbCheck( cudaStreamSynchronize(stream[i]) );

	for (int i = 1; i < NOSTREAMS-1; i++) {
         wbTime_start(Copy, msg("Copying output memory to the CPU - Stream", i-1));
         wbCheck( cudaMemcpyAsync(h_C + (i-1)*segSize, d_C[i-1], segSize*sizeof(float),cudaMemcpyDeviceToHost, stream[i-1]) );
         wbTime_stop(Copy, msg("Copying output memory to the CPU - Stream", i-1));

         wbTime_start(Compute, msg("Performing CUDA computation - Stream", i));
         vecAdd<<<gg, bb, 0, stream[i]>>>(d_A[i], d_B[i], d_C[i], segSize);
         wbTime_stop(Compute, msg("Performing CUDA computation - Stream", i));

 		 wbTime_start(GPU, msg("Copying input memory to the GPU - Stream", i+1));
         wbCheck( cudaMemcpyAsync(d_A[i+1], h_A + (i+1)*segSize, segSize*sizeof(float), cudaMemcpyHostToDevice, stream[i+1]) );
         wbCheck( cudaMemcpyAsync(d_B[i+1], h_B + (i+1)*segSize, segSize*sizeof(float), cudaMemcpyHostToDevice, stream[i+1]) );
         wbTime_stop(GPU, msg("Copying input memory to the GPU - Stream", i+1));

	     wbCheck( cudaStreamSynchronize(stream[i-1]) );
         wbCheck( cudaStreamSynchronize(stream[i]) );
         wbCheck( cudaStreamSynchronize(stream[i+1]) );
	}


	i = NOSTREAMS - 1;
	wbTime_start(Copy, msg("Copying output memory to the CPU - Stream", i-1));
    wbCheck( cudaMemcpyAsync(h_C + (i-1)*segSize, d_C[i-1], segSize*sizeof(float),cudaMemcpyDeviceToHost, stream[i-1]) );
    wbTime_stop(Copy, msg("Copying output memory to the CPU - Stream", i-1));

    wbTime_start(Compute, msg("Performing CUDA computation - Stream", i));
    vecAdd<<<gg, bb, 0, stream[i]>>>(d_A[i], d_B[i], d_C[i], segSize);
    wbTime_stop(Compute, msg("Performing CUDA computation - Stream", i));

	wbCheck( cudaStreamSynchronize(stream[i]) );

    wbTime_start(Copy, msg("Copying output memory to the CPU - Stream", i));
    wbCheck( cudaMemcpyAsync(h_C + i*segSize, d_C[i], segSize*sizeof(float),cudaMemcpyDeviceToHost, stream[i]) );
    wbTime_stop(Copy, msg("Copying output memory to the CPU - Stream", i));

    wbCheck( cudaStreamSynchronize(stream[i]) );

	cudaDeviceSynchronize();

	wbSolution(args, h_C, inputLength);

  //@@ Free the GPU memory here

    wbTime_start(GPU, "Freeing GPU Memory");
    for (int i = 0; i < NOSTREAMS; i++) {
        wbCheck( cudaFree( d_A[i] ) );
	    wbCheck( cudaFree( d_B[i] ) );
		wbCheck( cudaFree( d_C[i] ) );
	}
	wbTime_stop(GPU, "Freeing GPU Memory");


    free(h_A);
    free(h_B);
    free(h_C);

    return 0;
}
