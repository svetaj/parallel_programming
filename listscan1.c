// MP Scan
// Given a list (lst) of length n
// Output its prefix sum = {lst[0], lst[0] + lst[1], lst[0] + lst[1] + ... + lst[n-1]}

#include    <wb.h>

#define BLOCK_SIZE 512 //@@ You can change this

#define wbCheck(stmt) do {                                                    \
        cudaError_t err = stmt;                                               \
        if (err != cudaSuccess) {                                             \
            wbLog(ERROR, "Failed to run stmt ", #stmt);                       \
            wbLog(ERROR, "Got CUDA error ...  ", cudaGetErrorString(err));    \
            return -1;                                                        \
        }                                                                     \
    } while(0)


__device__ void to_shared(float *partialSum, float * input, int len) {

    unsigned int t = threadIdx.x;
    unsigned int start = 2*blockIdx.x*blockDim.x;

	// populate shared mem with two adjancent blocks
	if (start + t < len)
        atomicExch(&(partialSum[t]), input[start + t]);
	else
		atomicExch(&(partialSum[t]), 0.0);
	if (start + blockDim.x + t < len)
        atomicExch(&(partialSum[blockDim.x + t]), input[start + blockDim.x + t]);
	else
		atomicExch(&(partialSum[blockDim.x + t]), 0.0);
}

__device__ void reduction_phase(float *partialSum) {

	// Reducton phase
    for (int stride = 1; stride <= BLOCK_SIZE; stride *= 2) {
        int index = (threadIdx.x + 1) * stride*2 - 1;
        if(index < 2 * BLOCK_SIZE)
            atomicAdd(&(partialSum[index]), partialSum[index - stride]);
        __syncthreads();
     }
}

__device__ void post_reduction(float *partialSum) {

	// Post Reduction Reverse Phase
	for (int stride = BLOCK_SIZE/2; stride > 0; stride /= 2) {
         __syncthreads();
         int index = (threadIdx.x+1)*stride*2 - 1;
         if(index + stride < 2*BLOCK_SIZE)
              atomicAdd(&(partialSum[index + stride]), partialSum[index]);
        __syncthreads();
     }
}

__device__ void from_shared(float *partialSum, float * output, int len) {
	// populate output vector back from shared mem

	unsigned int t = threadIdx.x;
    unsigned int start = 2*blockIdx.x*blockDim.x;

	__syncthreads();
	if (start + t < len)
        atomicExch(&(output[start + t]), partialSum[t]);
	if (start + blockDim.x + t < len)
        atomicExch(&(output[start + blockDim.x + t]), partialSum[blockDim.x + t]);
}


__device__ void final_part(float *partialSum, float * output, int len) {
	unsigned int t = threadIdx.x;
	unsigned int b = blockIdx.x;

	if ((b+1)*2*BLOCK_SIZE+t < len)
	    atomicAdd(&(output[(b+1)*2*BLOCK_SIZE+t]), partialSum[b]);
	if ((b+1)*2*BLOCK_SIZE+BLOCK_SIZE+t < len)
		atomicAdd(&(output[(b+1)*2*BLOCK_SIZE+BLOCK_SIZE+t]), partialSum[b]);
	__syncthreads();
}

__device__ float pf[2*BLOCK_SIZE];


__device__ void second_phase(float *partialSum, float * output, int len) {
	unsigned int t = threadIdx.x;
	unsigned int g = gridDim.x;

	atomicExch(&(partialSum[t]), 0.0);
	atomicExch(&(partialSum[t+BLOCK_SIZE]), 0.0);
	__syncthreads();
	if (t < g)
	    atomicExch(&(partialSum[t]), output[(t+1)*2*BLOCK_SIZE-1]);
}

__global__ void scan(float * input, float * output, int len) {
    //@@ Modify the body of this function to complete the functionality of
    //@@ the scan on the device
    //@@ You may need multiple kernel calls; write your kernels before this
    //@@ function and call them from here

	unsigned int t = threadIdx.x;
	unsigned int b = blockIdx.x;
	__shared__ float partialSum[2*BLOCK_SIZE];
	
    to_shared(partialSum, input, len);
	__syncthreads();
	reduction_phase(partialSum);
	__syncthreads();
    post_reduction(partialSum);
	__syncthreads();
	from_shared(partialSum, output, len);
	__syncthreads();
}

__global__ void scan1(float * input, float * output, int len) {
	unsigned int t = threadIdx.x;
	unsigned int b = blockIdx.x;
	__shared__ float partialSum[2*BLOCK_SIZE];

	second_phase(partialSum, output, len);
	__syncthreads();
	reduction_phase(partialSum);
	__syncthreads();
    post_reduction(partialSum);
	__syncthreads();
    final_part(partialSum, output, len);
	__syncthreads();
}
	
int main(int argc, char ** argv) {
    wbArg_t args;
    float * hostInput; // The input 1D list
    float * hostOutput; // The output list
    float * deviceInput;
    float * deviceOutput;
    int numElements; // number of elements in the list

    args = wbArg_read(argc, argv);

    wbTime_start(Generic, "Importing data and creating memory on host");
    hostInput = (float *) wbImport(wbArg_getInputFile(args, 0), &numElements);
    hostOutput = (float*) malloc(numElements * sizeof(float));
    wbTime_stop(Generic, "Importing data and creating memory on host");

    wbLog(TRACE, "The number of input elements in the input is ", numElements);

    wbTime_start(GPU, "Allocating GPU memory.");
    wbCheck(cudaMalloc((void**)&deviceInput, numElements*sizeof(float)));
    wbCheck(cudaMalloc((void**)&deviceOutput, numElements*sizeof(float)));
    wbTime_stop(GPU, "Allocating GPU memory.");

    wbTime_start(GPU, "Clearing output memory.");
    wbCheck(cudaMemset(deviceOutput, 0, numElements*sizeof(float)));
    wbTime_stop(GPU, "Clearing output memory.");

    wbTime_start(GPU, "Copying input memory to the GPU.");
    wbCheck(cudaMemcpy(deviceInput, hostInput, numElements*sizeof(float), cudaMemcpyHostToDevice));
    wbTime_stop(GPU, "Copying input memory to the GPU.");

    //@@ Initialize the grid and block dimensions here

	int numGrid = numElements/(2*BLOCK_SIZE);
	if (numElements % (2*BLOCK_SIZE) > 0) numGrid++;
	numGrid = numGrid * 2;
	dim3 dimGrid(numGrid, 1, 1);
	dim3 dimBlock(BLOCK_SIZE, 1, 1);

    wbTime_start(Compute, "Performing CUDA computation");
    //@@ Modify this to complete the functionality of the scan
    //@@ on the device

	wbLog(TRACE, "Grid is ", numGrid);
    wbLog(TRACE, "Block is ", BLOCK_SIZE);

	scan<<<dimGrid,dimBlock>>>(deviceInput, deviceOutput, numElements);
    scan1<<<dimGrid,dimBlock>>>(deviceInput, deviceOutput, numElements);

    cudaDeviceSynchronize();
    wbTime_stop(Compute, "Performing CUDA computation");

    wbTime_start(Copy, "Copying output memory to the CPU");
    wbCheck(cudaMemcpy(hostOutput, deviceOutput, numElements*sizeof(float), cudaMemcpyDeviceToHost));
    wbTime_stop(Copy, "Copying output memory to the CPU");

    wbTime_start(GPU, "Freeing GPU Memory");
    cudaFree(deviceInput);
    cudaFree(deviceOutput);
    wbTime_stop(GPU, "Freeing GPU Memory");

    wbSolution(args, hostOutput, numElements);

    free(hostInput);
    free(hostOutput);

    return 0;
}
