#include    <wb.h>
#include "stdio.h"

#define wbCheck(stmt) do {                                                    \
        cudaError_t err = stmt;                                               \
        if (err != cudaSuccess) {                                             \
            wbLog(ERROR, "Failed to run stmt ", #stmt);                       \
            wbLog(ERROR, "Got CUDA error ...  ", cudaGetErrorString(err));    \
            return -1;                                                        \
        }                                                                     \
    } while(0)

//@@ INSERT CODE HERE

#define MASK_WIDTH  5
#define MASK_RADIUS MASK_WIDTH/2
#define O_TILE_WIDTH 16
#define BLOCK_WIDTH (O_TILE_WIDTH + MASK_WIDTH - 1)
#define DIM1 O_TILE_WIDTH
#define DIM2 BLOCK_WIDTH
#define clamp(x, start, end) (min(max((x), start), end))
#define DEBUG 0

__global__ void convolution_2D_kernel(float *P, float *N, int height,
                  int width, int channels, const float * __restrict__ M) {

	int img_x, img_y, img_s, i, j, idx, row_i, col_i;
	__shared__ float Ns[BLOCK_WIDTH][BLOCK_WIDTH];

	if (DEBUG == 1 && blockIdx.y == 0 && threadIdx.y == 0 && blockIdx.x == 0 && threadIdx.x == 0 && blockIdx.z == 0) {
		printf ("NUMBER OF THREADS PER ONE BLOCK blockDim.y=%d x blockDim.x=%d x blockDim.z=%d\n", blockDim.y, blockDim.x, blockDim.z);
		printf ("NUMBER OF BLOCKS gridDim.y=%d x gridDim.x=%d x gridDim.z=%d\n", gridDim.y, gridDim.x, gridDim.z);
	}		
	
    img_y = blockIdx.y * O_TILE_WIDTH + threadIdx.y;   // image apsolute y coordinate
    img_x = blockIdx.x * O_TILE_WIDTH + threadIdx.x;   // image apsolute x coordinate
	img_s = (img_y * width + img_x)*channels + blockIdx.z;

    row_i = img_y - MASK_RADIUS;                       // starting tile y coordinate
    col_i = img_x - MASK_RADIUS;                       // starting tile x coordinate
    idx = (row_i*width + col_i)*channels + blockIdx.z;

	// first step: we use ALL threads from "thread block" to populate tile
	// the size of tile should match thread block size (BLOCK_WIDTH = O_TILE_WIDTH + MASK_WIDTH -1) 
	if((row_i >= 0) && (row_i < height) && (col_i >= 0) && (col_i < width) ) {
        Ns[threadIdx.y][threadIdx.x] = N[idx];
    } else {
        Ns[threadIdx.y][threadIdx.x] = 0.0f;
    }

	// we sync all threads from block to assure successful tile population
	__syncthreads();

	if (DEBUG == 1 && blockIdx.y == 3 && threadIdx.y == 0 && blockIdx.x == 3 && threadIdx.x == 0 && blockIdx.z == 2) {
		printf("\nNs in block y x z %d %d %d \n", blockIdx.y, blockIdx.x, blockIdx.z);
		for(i = 0; i < BLOCK_WIDTH; i++) {
            for(j = 0; j < BLOCK_WIDTH; j++) {
                printf("%.2f ", Ns[i][j]);
            }
			printf("\n");
         }
		printf("\nN in block y x z %d %d %d (populated by %d x %d threads from same block)\n", 
			   blockIdx.y, blockIdx.x, blockIdx.z, blockDim.y, blockDim.x);
		for(i = 0; i < O_TILE_WIDTH; i++) {
            for(j = 0; j < O_TILE_WIDTH; j++) {
                printf("%.2f ", N[((img_y+i)*width + img_x+j)*channels + blockIdx.z]);
            }
			printf("\n");
         }
	}

	
	// second step: we use part of threads from block to calculate convolution
	// we use thread y, x ids as index and thread z ID as colour index. 
	// We calculate convolution for one "data block"
	// For simplicity, size of the image is divisible by data block size (O_TILE_WIDTH)
	
 	float output = 0.0f;
    if(threadIdx.y < O_TILE_WIDTH && threadIdx.x < O_TILE_WIDTH) {
         for(i = 0; i < MASK_WIDTH; i++) {
              for(j = 0; j < MASK_WIDTH; j++) {
                    output += M[i*MASK_WIDTH+j] * Ns[i+threadIdx.y][j+threadIdx.x];
              }
         }
	     if(img_y < height && img_x < width)
              P[img_s] = clamp(output, 0.0, 1.0);
     }

	// we sync all threads from block to assure successful output vector population	
	__syncthreads();
	
//----------------------------------------	
}




int main(int argc, char* argv[]) {
    wbArg_t args;
    int maskRows;
    int maskColumns;
    int imageChannels;
    int imageWidth;
    int imageHeight;
    char * inputImageFile;
    char * inputMaskFile;
    wbImage_t inputImage;
    wbImage_t outputImage;
    float * hostInputImageData;
    float * hostOutputImageData;
    float * hostMaskData;
    float * deviceInputImageData;
    float * deviceOutputImageData;
    float * deviceMaskData;

    args = wbArg_read(argc, argv); /* parse the input arguments */

    inputImageFile = wbArg_getInputFile(args, 0);
    inputMaskFile = wbArg_getInputFile(args, 1);

    inputImage = wbImport(inputImageFile);
    hostMaskData = (float *) wbImport(inputMaskFile, &maskRows, &maskColumns);

    assert(maskRows == 5); /* mask height is fixed to 5 in this mp */
    assert(maskColumns == 5); /* mask width is fixed to 5 in this mp */

    imageWidth = wbImage_getWidth(inputImage);
    imageHeight = wbImage_getHeight(inputImage);
    imageChannels = wbImage_getChannels(inputImage);

    outputImage = wbImage_new(imageWidth, imageHeight, imageChannels);

    hostInputImageData = wbImage_getData(inputImage);
    hostOutputImageData = wbImage_getData(outputImage);

    wbTime_start(GPU, "Doing GPU Computation (memory + compute)");

    wbTime_start(GPU, "Doing GPU memory allocation");
    cudaMalloc((void **) &deviceInputImageData, imageWidth * imageHeight * imageChannels * sizeof(float));
    cudaMalloc((void **) &deviceOutputImageData, imageWidth * imageHeight * imageChannels * sizeof(float));
    cudaMalloc((void **) &deviceMaskData, maskRows * maskColumns * sizeof(float));
    wbTime_stop(GPU, "Doing GPU memory allocation");


    wbTime_start(Copy, "Copying data to the GPU");
    cudaMemcpy(deviceInputImageData,
               hostInputImageData,
               imageWidth * imageHeight * imageChannels * sizeof(float),
               cudaMemcpyHostToDevice);
    cudaMemcpy(deviceMaskData,
               hostMaskData,
               maskRows * maskColumns * sizeof(float),
               cudaMemcpyHostToDevice);
    wbTime_stop(Copy, "Copying data to the GPU");


    wbTime_start(Compute, "Doing the computation on the GPU");
    //@@ INSERT CODE HERE

	if (DEBUG == 1) printf("imageWidth=%d imageHeight=%d\n", imageWidth, imageHeight);
    dim3 dimGrid(ceil((float)imageWidth/(float)DIM1), ceil((float)imageHeight/(float)DIM1), imageChannels);  
	dim3 dimBlock(DIM2, DIM2, 1);   
	convolution_2D_kernel<<<dimGrid,dimBlock>>> (deviceOutputImageData, deviceInputImageData,
												 imageHeight, imageWidth, imageChannels, deviceMaskData);

    wbTime_stop(Compute, "Doing the computation on the GPU");

    wbTime_start(Copy, "Copying data from the GPU");
    cudaMemcpy(hostOutputImageData,
               deviceOutputImageData,
               imageWidth * imageHeight * imageChannels * sizeof(float),
               cudaMemcpyDeviceToHost);

	
	wbTime_stop(Copy, "Copying data from the GPU");

    wbTime_stop(GPU, "Doing GPU Computation (memory + compute)");

    wbSolution(args, outputImage);

    cudaFree(deviceInputImageData);
    cudaFree(deviceOutputImageData);
    cudaFree(deviceMaskData);

    free(hostMaskData);
    wbImage_delete(outputImage);
    wbImage_delete(inputImage);

    return 0;
}
