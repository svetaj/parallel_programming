// Histogram Equalization

#include    <wb.h>

// Histogram length maches sizeof(unsigned char), 1 byte, 0 to 255
#define HISTOGRAM_LENGTH 256

//@@ insert code here

#define BLOCK_SIZE 256

#define wbCheck(stmt) do {                                                    \
        cudaError_t err = stmt;                                               \
        if (err != cudaSuccess) {                                             \
            wbLog(ERROR, "Failed to run stmt ", #stmt);                       \
            wbLog(ERROR, "Got CUDA error ...  ", cudaGetErrorString(err));    \
            return -1;                                                        \
        }                                                                     \
    } while(0)

__global__ void ucharCastImage(float * input, unsigned char * output, int len) {
	unsigned int tr = threadIdx.x;
	unsigned int bl = blockIdx.x;
	unsigned int di = blockDim.x;

	int ii = bl * di + tr;
	if (ii < len) {
          output[ii] = (unsigned char) (255 * input[ii]);
	}
}



__global__ void grayScale1(unsigned char *input, unsigned char *output, int width, int height) {
	unsigned int tx = threadIdx.x;
	unsigned int bx = blockIdx.x;
	unsigned int dx = blockDim.x;
	unsigned int ty = threadIdx.y;
	unsigned int by = blockIdx.y;
	unsigned int dy = blockDim.y;

	float r, g, b;

	int idx = tx * width + ty;
	if (tx < height && ty < width) {
	        r = input[3*idx];
            g = input[3*idx + 1];
            b = input[3*idx + 2];
            output[idx] = (unsigned char) (0.21*r + 0.71*g + 0.07*b);
	}
    __syncthreads();
}

__global__ void grayScale(unsigned char *input, unsigned char *output, int size) {
	unsigned int tx = threadIdx.x;
	unsigned int bx = blockIdx.x;
	unsigned int dx = blockDim.x;

	float r, g, b;

	int i = bx * dx + tx;
	if (i < size) {
	     r = input[3*i];
         g = input[3*i + 1];
         b = input[3*i + 2];
         output[i] = (unsigned char) (0.21*r + 0.71*g + 0.07*b);
	}
    __syncthreads();
}


__global__ void histo_kernel_serial(unsigned char *buffer, unsigned int *histo, int size) {
	unsigned int tr = threadIdx.x;
	unsigned int bl = blockIdx.x;

	if (tr == 0 && bl == 0) {
		for (int i=0; i<HISTOGRAM_LENGTH; i++)
		    histo[i] = 0;
		for (int i=0; i<size; i++)
		    histo[buffer[i]]++;
    }
}



__global__ void histo_kernel(unsigned char *buffer, unsigned int *histo, int size) {
	unsigned int tr = threadIdx.x;
	unsigned int bl = blockIdx.x;
	unsigned int di = blockDim.x;
	unsigned int gr = gridDim.x;

     __shared__ unsigned int private_histo[HISTOGRAM_LENGTH];
     if (tr < HISTOGRAM_LENGTH) private_histo[tr] = 0;
     __syncthreads();
	int i = tr + bl * di;
    // stride is total number of threads
    int stride = bl * gr;

    // all threads from same block write into same shared memory segment private_histo
//	while (i < size) {
	if (i < size) {
		atomicAdd(&(private_histo[buffer[i]]), 1);
//        i += stride;
    }

    // wait for all other threads in the block to finish
    __syncthreads();
	// all threads from all blocks write into result
    if (tr < HISTOGRAM_LENGTH) {
        atomicAdd( &(histo[tr]), private_histo[tr] );
    }
}

// p is the probability of a pixel to be in a histogram bin
// #define p(x,w,h) (x / (w * h))
#define p(x,y) (x / y)
#define clamp(x, start, end) (min(max((x), start), end))
#define correct_color(val) (clamp(255*(cdf[val] - cdfmin)/(1 - cdfmin), 0, 255))

__global__ void cumulativeDistr(unsigned int *hist, float *cdf, int size) {
	unsigned int tr = threadIdx.x;
	unsigned int bl = blockIdx.x;

	if (tr == 0 && bl == 0) {
 	    unsigned int sum = 0;
		for (int i = 0; i < HISTOGRAM_LENGTH; i++) {
             sum += hist[i];
			 cdf[i] = sum;
		}

		for (int j = 0; j < HISTOGRAM_LENGTH; j++) {
			 cdf[j] /= (float) size;
		}
	}
}


__global__ void histogramEqualization(unsigned char * ucharImage, float * outputImage, float *cdf, int len) {
	unsigned int tr = threadIdx.x;
	unsigned int bl = blockIdx.x;
	unsigned int di = blockDim.x;

	long ii = bl * di + tr;
	if (ii < len) {
		 ucharImage[ii] = clamp((unsigned char)(255*( (cdf[ucharImage[ii]] - cdf[0])/(1.0 - cdf[0])) ), 0, 255);
         outputImage[ii] = (float) (ucharImage[ii]/255.0);
	}
	__syncthreads();
}



int main(int argc, char ** argv) {
    wbArg_t args;
    int imageWidth;
    int imageHeight;
    int imageChannels;
    wbImage_t inputImage;
    wbImage_t outputImage;
    float * hostInputImageData;
    float * hostOutputImageData;
    const char * inputImageFile;

    //@@ Insert more code here

    args = wbArg_read(argc, argv); /* parse the input arguments */

    inputImageFile = wbArg_getInputFile(args, 0);

    wbTime_start(Generic, "Importing data and creating memory on host");
    inputImage = wbImport(inputImageFile);
    imageWidth = wbImage_getWidth(inputImage);
    imageHeight = wbImage_getHeight(inputImage);
    imageChannels = wbImage_getChannels(inputImage);
    outputImage = wbImage_new(imageWidth, imageHeight, imageChannels);
    wbTime_stop(Generic, "Importing data and creating memory on host");

    //@@ insert code here

	wbLog(TRACE, "Image width ", imageWidth);
    wbLog(TRACE, "Image height ", imageHeight);
    wbLog(TRACE, "Image channels ", imageChannels);
    wbLog(TRACE, "sizeof(int) ", sizeof(int));
    wbLog(TRACE, "sizeof(long) ", sizeof(long));

	hostInputImageData = wbImage_getData(inputImage);
    hostOutputImageData = wbImage_getData(outputImage);

	int imageSize = imageWidth * imageHeight * imageChannels;
	int imageSizeG = imageWidth * imageHeight;
	unsigned char * hostOutputUC;
	unsigned char * hostGrayImage;
	unsigned int *hostHist;
	float *hostCDF;
	hostOutputUC = (unsigned char*) malloc(imageSize * sizeof(unsigned char));
	hostGrayImage = (unsigned char*) malloc(imageSizeG * sizeof(unsigned char));
	hostHist = (unsigned int*) malloc(HISTOGRAM_LENGTH * sizeof(unsigned int));
	hostCDF = (float *) malloc(HISTOGRAM_LENGTH * sizeof(float));
    float * deviceInputF;
    float * deviceOutputF;
    unsigned char * deviceOutputUC;
    unsigned char * deviceGrayImage;
    unsigned int * deviceHistogram;
    float * cumDist;

	wbTime_start(GPU, "Allocating GPU memory.");
    wbCheck(cudaMalloc((void**)&deviceInputF, imageSize*sizeof(float)));
    wbCheck(cudaMalloc((void**)&deviceOutputF, imageSize*sizeof(float)));
    wbCheck(cudaMalloc((void**)&deviceOutputUC, imageSize*sizeof(unsigned char)));
    wbCheck(cudaMalloc((void**)&deviceGrayImage, imageSizeG*sizeof(unsigned char)));
    wbCheck(cudaMalloc((void**)&deviceHistogram, HISTOGRAM_LENGTH *sizeof(unsigned int)));
    wbCheck(cudaMalloc((void**)&cumDist, HISTOGRAM_LENGTH *sizeof(float)));
    wbTime_stop(GPU, "Allocating GPU memory.");

	wbTime_start(Copy, "Copying data to the GPU");
    wbCheck(cudaMemcpy(deviceInputF, hostInputImageData, imageSize * sizeof(float), cudaMemcpyHostToDevice));
    wbTime_stop(Copy, "Copying data to the GPU");

    printf("original float image\n");
	for (int j = 0; j < HISTOGRAM_LENGTH; j++)
		printf("%.2f ", hostInputImageData[j]);
	printf("\n");


	//@@ Initialize the grid and block dimensions here

	int numGrid = imageSize/BLOCK_SIZE;
	if (imageSize % BLOCK_SIZE > 0) numGrid++;
	dim3 dimGrid(numGrid, 1, 1);
	dim3 dimBlock(BLOCK_SIZE, 1, 1);

    wbTime_start(Compute, "Performing CUDA computation");
    //@@ Modify this to complete the functionality of the scan
    //@@ on the device

	wbLog(TRACE, "Grid is ", numGrid);
    wbLog(TRACE, "Block is ", BLOCK_SIZE);

    wbTime_start(Copy, "Unsigned char conversion");
	ucharCastImage<<<dimGrid,dimBlock>>>(deviceInputF, deviceOutputUC, imageSize);
    wbTime_stop(Copy, "Unsigned char conversion");

	wbTime_start(Copy, "Copying unsigned char image to the CPU");
    wbCheck(cudaMemcpy(hostOutputUC, deviceOutputUC, imageSize*sizeof(unsigned char), cudaMemcpyDeviceToHost));
    wbTime_stop(Copy, "Copying unsigned char image to the CPU");

    printf("unsigned char image\n");
	for (int j = 0; j < 20; j++)
		printf("%d ", hostOutputUC[j]);
	printf("\n");
	for (int j = imageSize-20; j < imageSize; j++)
		printf("%d ", hostOutputUC[j]);
	printf("\n");

	int numGridx = imageHeight/BLOCK_SIZE;
	if (imageHeight % BLOCK_SIZE > 0) numGridx++;
	int numGridy = imageWidth/BLOCK_SIZE;
	if (imageWidth % BLOCK_SIZE > 0) numGridy++;
	dim3 dimGridg(1, 1, 1);
	dim3 dimBlockg(BLOCK_SIZE/2, BLOCK_SIZE/2, 1);

    wbTime_start(Copy, "Gray scale conversion");
	grayScale<<<dimGrid,dimBlock>>>(deviceOutputUC, deviceGrayImage, imageSizeG);
    wbTime_stop(Copy, "Gray scale conversion");

	wbTime_start(Copy, "Copying gray image to the CPU");
    wbCheck(cudaMemcpy(hostGrayImage, deviceGrayImage, imageSizeG*sizeof(unsigned char), cudaMemcpyDeviceToHost));
    wbTime_stop(Copy, "Copying gray image to the CPU");

    printf("gray image\n");
	for (int j = 0; j < 20; j++)
		printf("%d ", hostGrayImage[j]);
	printf("\n");
	for (int j = imageSizeG-20; j < imageSizeG; j++)
		printf("%d ", hostGrayImage[j]);
	printf("\n");

	int numGrid1 = imageSize/(4*BLOCK_SIZE);
	if (imageSize % (4*BLOCK_SIZE) > 0) numGrid1++;
	dim3 dimGrid1(numGrid1, 1, 1);
	dim3 dimBlock1((4*BLOCK_SIZE), 1, 1);

    wbTime_start(Copy, "Histogram computation");
//	histo_kernel<<<dimGrid1,dimBlock1>>>(deviceGrayImage, deviceHistogram, imageSizeG);
	histo_kernel_serial<<<1,1>>>(deviceGrayImage, deviceHistogram, imageSizeG);
    wbTime_stop(Copy, "Histogram computation");

	wbTime_start(Copy, "Copying histogram to the CPU");
    wbCheck(cudaMemcpy(hostHist, deviceHistogram, HISTOGRAM_LENGTH*sizeof(unsigned int), cudaMemcpyDeviceToHost));
    wbTime_stop(Copy, "Copying histogram to the CPU");

    printf("histogram\n");
	int sum = 0;
	for (int i = 0; i < HISTOGRAM_LENGTH; i++) {
		printf("%d ", hostHist[i]);
	    if (i<125) sum+=hostHist[i];
	}
	printf("\n");
	printf("################## sum 0..124 =%d\n", sum);
	printf("\n");

    wbTime_start(Copy, "Cumulative distribution");
	cumulativeDistr<<<1,1>>>(deviceHistogram, cumDist, imageSizeG);
    wbTime_stop(Copy, "Cumulative distribution");

	wbTime_start(Copy, "Copying cdf to the CPU");
    wbCheck(cudaMemcpy(hostCDF, cumDist, HISTOGRAM_LENGTH*sizeof(float), cudaMemcpyDeviceToHost));
    wbTime_stop(Copy, "Copying cdf to the CPU");

    printf("cdf\n");
	for (int i = 0; i <= HISTOGRAM_LENGTH; i++)
		printf("%.2f ", hostCDF[i]);
	printf("\n");

	wbTime_start(Copy, "Histogram equalization");
	histogramEqualization<<<dimGrid,dimBlock>>>(deviceOutputUC, deviceOutputF, cumDist, imageSize);
    wbTime_stop(Copy, "Histogram equalization");

    cudaDeviceSynchronize();
    wbTime_stop(Compute, "Performing CUDA computation");

    wbTime_start(Copy, "Copying output memory to the CPU");
    wbCheck(cudaMemcpy(hostOutputImageData, deviceOutputF, imageSize*sizeof(float), cudaMemcpyDeviceToHost));
    wbTime_stop(Copy, "Copying output memory to the CPU");

	printf("Equalized float image\n");
	for (int j = 0; j < 20; j++)
		printf("%d ", hostOutputImageData[j]);
	printf("\n");
	for (int j = imageSize-20; j < imageSize; j++)
		printf("%d ", hostOutputImageData[j]);
	printf("\n");

	wbSolution(args, outputImage);

    //@@ insert code here

	wbTime_start(GPU, "Freeing GPU Memory");
	cudaFree(deviceInputF);
    cudaFree(deviceOutputF);
    cudaFree(deviceOutputUC);
    cudaFree(deviceGrayImage);
    cudaFree(deviceHistogram);
	cudaFree(cumDist);
    wbTime_stop(GPU, "Freeing GPU Memory");

	free(hostHist);
    free(hostOutputUC);
    free(hostGrayImage);
    wbImage_delete(inputImage);
    wbImage_delete(outputImage);

    return 0;
}

