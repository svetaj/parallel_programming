#include    <wb.h>


#define wbCheck(stmt) do {                                                    \
        cudaError_t err = stmt;                                               \
        if (err != cudaSuccess) {                                             \
            wbLog(ERROR, "Failed to run stmt ", #stmt);                       \
            wbLog(ERROR, "Got CUDA error ...  ", cudaGetErrorString(err));    \
            return -1;                                                        \
        }                                                                     \
    } while(0)

//@@ INSERT CODE HERE

#define MASK_WIDTH  5
#define MASK_RADIUS MASK_WIDTH/2
#define O_TILE_WIDTH 12
#define TILE_WIDTH 16
#define BLOCK_WIDTH (O_TILE_WIDTH + 4)
#define DIMNS (TILE_WIDTH + MASK_WIDTH - 1)
#define clamp(x, start, end) (min(max((x), start), end))

__global__ void convolution_2D_kernel(float *P, float *N, int height,
                  int width, int channels, const float * __restrict__ M) {


//---------------------------------------

   __shared__ float Ns[DIMNS][DIMNS];
   int k, dind, dd, dx, dy, sx, sy, ss, ix, iy;
   float sum = 0.0;

   for (k = 0; k < channels; k++) {
	   dd = threadIdx.y * TILE_WIDTH + threadIdx.x;
	   dx = (threadIdx.y * TILE_WIDTH + threadIdx.x) / DIMNS;
       dy = (threadIdx.y * TILE_WIDTH + threadIdx.x) % DIMNS;
       sy = blockIdx.y * TILE_WIDTH + dy - MASK_RADIUS,
       sx = blockIdx.x * TILE_WIDTH + dx - MASK_RADIUS,
       src = (sy * width + sy) * channels + k;

       if (sy >= 0 && sy < height && sx >= 0 && sx < width)
	       Ns[dy][dx] = I[src];
	   else
           Ns[dy][dx] = 0;

       dest = threadIdx.y * TILE_WIDTH
              + threadIdx.x + TILE_WIDTH * TILE_WIDTH;
       dy = threadIdx.y * TILE_WIDTH
              + threadIdx.x + TILE_WIDTH * TILE_WIDTH / DIMNS;
       dx = threadIdx.y * TILE_WIDTH
              + threadIdx.x + TILE_WIDTH * TILE_WIDTH % DIMNS;
       sy = blockIdx.y * TILE_WIDTH + dy - MASK_RADIUS,
       sx = blockIdx.x * TILE_WIDTH + dx - MASK_RADIUS,
       src = (sy * width + sy) * channels + k;
       if (dy < DIMNS) {
           if (sy >= 0 && sy < height && sx >= 0 && sx < width)
	           Ns[dy][dx] = I[src];
	       else
               Ns[dy][dx] = 0;
           }
       __syncthreads();

       sum = 0.0;
	   for (iy = 0; iy < MASK_WIDTH; iy++)
	            for (ix = 0; ix < MASK_WIDTH; ix++)
	               sum += Ns[threadIdx.y + iy][threadIdx.x + ix] * M[iy * MASK_WIDTH + ix];
	   iy = blockIdx.y * TILE_WIDTH + threadIdx.y;
	   ix = blockIdx.x * TILE_WIDTH + threadIdx.x;
	   if (iy < height && ix < width)
	       P[(iy * width + ix) * channels + k] = clamp(sum);
	    __syncthreads();
   }
}


//--------------------------------------

//    int maskWidth = 5;
//    int maskRadius = maskWidth/2;   // this is integer division, so the result is
//    int i, j, k, x, y, xOffset, yOffset;
//    float accum, imagePixel, maskValue;
//
//    for (i = 0; i < height; i++) {
//       for (j = 0; j < width; j++) {
//           for (k = 0; k < channels; k++) {
//               accum = 0.0;
//               for(y = -maskRadius; y <= maskRadius; y++) {
//                   for( x = -maskRadius; x <= maskRadius; x++) {
//                       xOffset = j + x;
//                       yOffset = i + y;
//                       if (xOffset >= 0 && xOffset < width && yOffset >= 0 && yOffset < height) {
//                           imagePixel = N[(yOffset * width + xOffset) * channels + k];
//                           maskValue = M[(y+maskRadius)*maskWidth+x+maskRadius];
//                           accum += imagePixel * maskValue;
//                       }
//                    }
//                }
//                // pixels are in the range of 0 to 1
//                P[(i * width + j)*channels + k] = clamp(accum, 0.0, 1.0);
//            }
//
//	    }
//	}




int main(int argc, char* argv[]) {
    wbArg_t args;
    int maskRows;
    int maskColumns;
    int imageChannels;
    int imageWidth;
    int imageHeight;
    char * inputImageFile;
    char * inputMaskFile;
    wbImage_t inputImage;
    wbImage_t outputImage;
    float * hostInputImageData;
    float * hostOutputImageData;
    float * hostMaskData;
    float * deviceInputImageData;
    float * deviceOutputImageData;
    float * deviceMaskData;

    args = wbArg_read(argc, argv); /* parse the input arguments */

    inputImageFile = wbArg_getInputFile(args, 0);
    inputMaskFile = wbArg_getInputFile(args, 1);

    inputImage = wbImport(inputImageFile);
    hostMaskData = (float *) wbImport(inputMaskFile, &maskRows, &maskColumns);

    assert(maskRows == 5); /* mask height is fixed to 5 in this mp */
    assert(maskColumns == 5); /* mask width is fixed to 5 in this mp */

    imageWidth = wbImage_getWidth(inputImage);
    imageHeight = wbImage_getHeight(inputImage);
    imageChannels = wbImage_getChannels(inputImage);

    outputImage = wbImage_new(imageWidth, imageHeight, imageChannels);

    hostInputImageData = wbImage_getData(inputImage);
    hostOutputImageData = wbImage_getData(outputImage);

    wbTime_start(GPU, "Doing GPU Computation (memory + compute)");

    wbTime_start(GPU, "Doing GPU memory allocation");
    cudaMalloc((void **) &deviceInputImageData, imageWidth * imageHeight * imageChannels * sizeof(float));
    cudaMalloc((void **) &deviceOutputImageData, imageWidth * imageHeight * imageChannels * sizeof(float));
    cudaMalloc((void **) &deviceMaskData, maskRows * maskColumns * sizeof(float));
    wbTime_stop(GPU, "Doing GPU memory allocation");


    wbTime_start(Copy, "Copying data to the GPU");
    cudaMemcpy(deviceInputImageData,
               hostInputImageData,
               imageWidth * imageHeight * imageChannels * sizeof(float),
               cudaMemcpyHostToDevice);
    cudaMemcpy(deviceMaskData,
               hostMaskData,
               maskRows * maskColumns * sizeof(float),
               cudaMemcpyHostToDevice);
    wbTime_stop(Copy, "Copying data to the GPU");


    wbTime_start(Compute, "Doing the computation on the GPU");
    //@@ INSERT CODE HERE

	dim3 dimGrid(ceil((float)imageWidth/TILE_WIDTH), ceil((float)imageHeight/TILE_WIDTH));
    dim3 dimBlock(TILE_WIDTH, TILE_WIDTH, 1);

	// dim3 dimBlock(BLOCK_WIDTH,BLOCK_WIDTH);
    // dim3 dimGrid((wbImage_getWidth(deviceInputImageData)-1)/O_TILE_WIDTH+1, (wbImage_getHeight(deviceInputImageData)-1)/O_TILE_WIDTH+1, 1);
	convolution_2D_kernel<<<dimGrid,dimBlock>>> (deviceOutputImageData, deviceInputImageData,
												 imageHeight, imageWidth, imageChannels, deviceMaskData);

    wbTime_stop(Compute, "Doing the computation on the GPU");


    wbTime_start(Copy, "Copying data from the GPU");
    cudaMemcpy(hostOutputImageData,
               deviceOutputImageData,
               imageWidth * imageHeight * imageChannels * sizeof(float),
               cudaMemcpyDeviceToHost);
    wbTime_stop(Copy, "Copying data from the GPU");

    wbTime_stop(GPU, "Doing GPU Computation (memory + compute)");

    wbSolution(args, outputImage);

    cudaFree(deviceInputImageData);
    cudaFree(deviceOutputImageData);
    cudaFree(deviceMaskData);

    free(hostMaskData);
    wbImage_delete(outputImage);
    wbImage_delete(inputImage);

    return 0;
}
