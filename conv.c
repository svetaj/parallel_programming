#include    <wb.h>


#define wbCheck(stmt) do {                                                    \
        cudaError_t err = stmt;                                               \
        if (err != cudaSuccess) {                                             \
            wbLog(ERROR, "Failed to run stmt ", #stmt);                       \
            wbLog(ERROR, "Got CUDA error ...  ", cudaGetErrorString(err));    \
            return -1;                                                        \
        }                                                                     \
    } while(0)

#define Mask_width  5
#define MASK_WIDTH  5
#define Mask_radius Mask_width/2

#define O_TILE_WIDTH 12
#define TILE_WIDTH 16
#define BLOCK_WIDTH (O_TILE_WIDTH + 4)

//@@ INSERT CODE HERE

#define clamp(x, start, end) (min(max((x), start), end))



__global__ void convolution_2D_kernel(float *P, float *N, int height, int width, int channels, const float * __restrict__ M) {

    int maskWidth = 5;
    int maskRadius = maskWidth/2;   // this is integer division, so the result is
    int i, j, k, x, y, xOffset, yOffset;
    float accum, imagePixel, maskValue;

//---------------------------------------

	int tx = threadIdx.x;
    int ty = threadIdx.y;
    int row_o = blockIdx.y*O_TILE_WIDTH + ty;
    int col_o = blockIdx.x*O_TILE_WIDTH + tx;
    int row_i = row_o - 2;
    int col_i = col_o - 2;
	if((row_i >= 0) && (row_i < height) && (col_i >= 0) && (col_i < width) ) {
         Ns[ty][tx] = N[row_i*width + col_i];
    } else {
        Ns[ty][tx] = 0.0f;
    }
	float output = 0.0f;
    if (ty < O_TILE_WIDTH && tx < O_TILE_WIDTH) {
        for (i = 0; i < MASK_WIDTH; i++) {
            for (j = 0; j < MASK_WIDTH; j++) {
                 output += M[i][j] * Ns[i+ty][j+tx];
            }
        }
	}
	if(row_o < height && col_o < width)
         data[row_o*width + col_o] = output;

//--------------------------------------

    for (i = 0; i < height; i++) {
       for (j = 0; j < width; j++) {
           for (k = 0; k < channels; k++) {
               accum = 0.0;
               for(y = -maskRadius; y <= maskRadius; y++) {
                   for( x = -maskRadius; x <= maskRadius; x++) {
                       xOffset = j + x;
                       yOffset = i + y;
                       if (xOffset >= 0 && xOffset < width && yOffset >= 0 && yOffset < height) {
                           imagePixel = N[(yOffset * width + xOffset) * channels + k];
                           maskValue = M[(y+maskRadius)*maskWidth+x+maskRadius];
                           accum += imagePixel * maskValue;
                       }
                    }
                }
                // pixels are in the range of 0 to 1
                P[(i * width + j)*channels + k] = clamp(accum, 0.0, 1.0);
            }

	    }
	}
}

//------------------------------------------------------------
// https://gist.github.com/wh5a/4641641
//------------------------------------------------------------
// __global__ void convolution_2D_kernel(float *P,
// float *N, int height, int width, int channels,
// const float * __restrict__ M) {
//	float *data = P;
//	// Shifting from output coordinates to input coordinate
//	int tx = threadIdx.x;
//    int ty = threadIdx.y;
//    int row_o = blockIdx.y*O_TILE_WIDTH + ty;
//	int col_o = blockIdx.x*O_TILE_WIDTH + tx;
//    int row_i = row_o - 2;
//    int col_i = col_o - 2;
//	int i, j;
//
//	// Taking Care of Boundaries (1 channel example)
//	if((row_i >= 0) && (row_i < height) && (col_i >= 0) && (col_i < width) ) {
//        Ns[ty][tx] = data[row_i*width + col_i];
//    } else {
//        Ns[ty][tx] = 0.0f;
//    }
//
//	// Some threads do not participate in calculating output. (1 channel example)
//	float output = 0.0f;
//    if(ty < O_TILE_WIDTH && tx < O_TILE_WIDTH) {
//        for(i = 0; i < MASK_WIDTH; i++) {
//             for(j = 0; j < MASK_WIDTH; j++) {
//                   output += M[i][j] * Ns[i+ty][j+tx];
//             }
//        }
//	}
//
//    // Some threads do not write output (1 channel example)
//	if(row_o < height && col_o < width)
//         data[row_o*width + col_o] = output;
//}



int main(int argc, char* argv[]) {
    wbArg_t args;
    int maskRows;
    int maskColumns;
    int imageChannels;
    int imageWidth;
    int imageHeight;
    char * inputImageFile;
    char * inputMaskFile;
    wbImage_t inputImage;
    wbImage_t outputImage;
    float * hostInputImageData;
    float * hostOutputImageData;
    float * hostMaskData;
    float * deviceInputImageData;
    float * deviceOutputImageData;
    float * deviceMaskData;

    args = wbArg_read(argc, argv); /* parse the input arguments */

    inputImageFile = wbArg_getInputFile(args, 0);
    inputMaskFile = wbArg_getInputFile(args, 1);

    inputImage = wbImport(inputImageFile);
    hostMaskData = (float *) wbImport(inputMaskFile, &maskRows, &maskColumns);

    assert(maskRows == 5); /* mask height is fixed to 5 in this mp */
    assert(maskColumns == 5); /* mask width is fixed to 5 in this mp */

    imageWidth = wbImage_getWidth(inputImage);
    imageHeight = wbImage_getHeight(inputImage);
    imageChannels = wbImage_getChannels(inputImage);

    outputImage = wbImage_new(imageWidth, imageHeight, imageChannels);

    hostInputImageData = wbImage_getData(inputImage);
    hostOutputImageData = wbImage_getData(outputImage);

    wbTime_start(GPU, "Doing GPU Computation (memory + compute)");

    wbTime_start(GPU, "Doing GPU memory allocation");
    cudaMalloc((void **) &deviceInputImageData, imageWidth * imageHeight * imageChannels * sizeof(float));
    cudaMalloc((void **) &deviceOutputImageData, imageWidth * imageHeight * imageChannels * sizeof(float));
    cudaMalloc((void **) &deviceMaskData, maskRows * maskColumns * sizeof(float));
    wbTime_stop(GPU, "Doing GPU memory allocation");


    wbTime_start(Copy, "Copying data to the GPU");
    cudaMemcpy(deviceInputImageData,
               hostInputImageData,
               imageWidth * imageHeight * imageChannels * sizeof(float),
               cudaMemcpyHostToDevice);
    cudaMemcpy(deviceMaskData,
               hostMaskData,
               maskRows * maskColumns * sizeof(float),
               cudaMemcpyHostToDevice);
    wbTime_stop(Copy, "Copying data to the GPU");


    wbTime_start(Compute, "Doing the computation on the GPU");
    //@@ INSERT CODE HERE

	dim3 dimGrid(ceil((float)imageWidth/TILE_WIDTH), ceil((float)imageHeight/TILE_WIDTH));
    dim3 dimBlock(TILE_WIDTH, TILE_WIDTH, 1);

	// dim3 dimBlock(BLOCK_WIDTH,BLOCK_WIDTH);
    // dim3 dimGrid((wbImage_getWidth(deviceInputImageData)-1)/O_TILE_WIDTH+1, (wbImage_getHeight(deviceInputImageData)-1)/O_TILE_WIDTH+1, 1);
	convolution_2D_kernel<<<dimGrid,dimBlock>>> (deviceOutputImageData, deviceInputImageData,
												 imageHeight, imageWidth, imageChannels, deviceMaskData);

    wbTime_stop(Compute, "Doing the computation on the GPU");


    wbTime_start(Copy, "Copying data from the GPU");
    cudaMemcpy(hostOutputImageData,
               deviceOutputImageData,
               imageWidth * imageHeight * imageChannels * sizeof(float),
               cudaMemcpyDeviceToHost);
    wbTime_stop(Copy, "Copying data from the GPU");

    wbTime_stop(GPU, "Doing GPU Computation (memory + compute)");

    wbSolution(args, outputImage);

    cudaFree(deviceInputImageData);
    cudaFree(deviceOutputImageData);
    cudaFree(deviceMaskData);

    free(hostMaskData);
    wbImage_delete(outputImage);
    wbImage_delete(inputImage);

    return 0;
}
